from fastapi import FastAPI
from routers import status, transcript

app = FastAPI()

app.include_router(status.router)
app.include_router(transcript.router)