FROM python:latest

WORKDIR /app

COPY requirements.txt .
COPY . .

RUN pip install -r requirements.txt

CMD [ "python script.py" ]