import whisper
from config.whisper import WHISPER

class Transcript:
    def __init__(self, path):
        self._language = WHISPER["LANGUAGE"]
        self._model = whisper.load_model(
            WHISPER["MODEL"],
            device=WHISPER["DEVICE"],
            language=self._language
        )

    def transcribe(self, audio):
        """Transcribe an audio file to text using whisper."""
        return self._model.transcribe(audio)