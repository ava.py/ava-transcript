"""Whisper configuration file."""
from utils.get_env import get_env

WHISPER = {
  "LANGUAGE": get_env("WHISPER_LANGUAGE", cast_as=str),
  "DEVICE": get_env("WHISPER_DEVICE", cast_as=str),
  "MODEL": get_env("WHISPER_MODEL", cast_as=str),
}