"""Shared configuration file."""
from utils.get_env import get_env

APP = {
  "VERSION": get_env("API_VERSION", cast_as=str),
}

ERROR_MESSAGES = {
  "NOT_FOUND": "The requested resource was not found",
  "BAD_REQUEST": "The request was malformed",
}