from fastapi import APIRouter
from enum import Enum
from pydantic import BaseModel
from config.shared import APP, ERROR_MESSAGES

router = APIRouter(
  prefix="/status",
  tags=["status"],
  responses={404: {"message": ERROR_MESSAGES["NOT_FOUND"]}}
)

class StatusEnum(str, Enum):
  OK = "OK"
  KO = "KO"

class StatusResponse(BaseModel):
  status: StatusEnum
  version: str

@router.get("/")
def status() -> StatusResponse:
  return {
    "status": StatusEnum.OK,
    "version": APP["VERSION"]
  }