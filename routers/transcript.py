from fastapi import APIRouter, UploadFile, HTTPException
from pydantic import BaseModel
from classes.transcript import Transcript
from config.shared import ERROR_MESSAGES

router = APIRouter(
  prefix="/api/v1/transcript",
  tags=["transcript"],
  responses={
    400: {"message": ERROR_MESSAGES["BAD_REQUEST"]},
    404: {"message": ERROR_MESSAGES["NOT_FOUND"]}
  }
)

class PostTranscript(BaseModel):
  transcript: str

@router.post('/')
def transcript(upload: UploadFile) -> PostTranscript:
  allowed_extensions = ["wav", "mp3"]
  extension = upload.filename.split(".")[-1]

  if extension not in allowed_extensions:
    HTTPException(status_code=400, detail=f"File extension {extension} is not allowed. Allowed extensions are {allowed_extensions}")
  
  return {
    "transcript": Transcript().transcribe(upload.file)
  }
